import { NgModule, Optional, SkipSelf } from "@angular/core";
import { CommonModule } from "@angular/common";
import { GameProcessService } from "./services/gameProcess.service";
import { PlayService } from "./services/play.service";
import { ServerRequestService } from "./services/serverRequest.service";
import { TraductionService } from "./services/traduction.service";

@NgModule({
  declarations: [],
  imports: [CommonModule],
  providers: [GameProcessService, PlayService, ServerRequestService, TraductionService]
})
export class CoreModule {
  constructor(@Optional() @SkipSelf() parentModule: CoreModule) {
    if (parentModule) {
      throw new Error("CoreModule is already loaded. Import it in the AppModule only");
    }
  }
}
