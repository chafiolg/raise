import { Injectable } from "@angular/core";
import { Router } from "@angular/router";
import { Game } from "src/app/globalModel/game";
import { Player } from "src/app/globalModel/player";

import { ServerRequestService } from "./serverRequest.service";

@Injectable({
  providedIn: "root"
})
export class GameProcessService {
  //pop up gestion du menu home apparassant lorsque leur valeur = true
  createWindowVisibility: boolean = false;
  joinWindowVisibility: boolean = false;
  launcherWindowVisibility: boolean = false;
  //objet game contenant les informations de la partie
  currentGame: Game;
  //object contenant les informations du joueur actif
  myPlayer: Player;

  constructor(private serverRequestService: ServerRequestService, private router: Router) {
  }

  //fonction de création de la partie lancé par le formulaire de création
  //enregistrement des informations dans la variable currentGame puis en base
  //name : le nom donné à la partie
  //password : son mot de passe, ou null si la partie est publique
  createTheGame(name: string, password: string) {
    let newId = this.createId(10); // l'id est unique :   839 million de milliards d'id différent...

    this.currentGame = new Game({
      id: newId,
      name: name,
      password: password
    });

    this.serverRequestService.createGameOnBase(this.currentGame);
  }

  //créaton d'un id sur <number> caractère avec 62^10 possibilités de code
  createId(length: number = 10) {
    let result = "";
    let characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    let charactersLength = characters.length;
    for (let i = 0; i < length; i++) {
      result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
  }

  //mise à jour des données de la partie en base
  //id : id de la partie à mettre à jour
  //callback : éventuelle fonction de callback lancé après la maj
  async updateCurrentGameById(id: string) {
    this.currentGame = new Game(await this.serverRequestService.getGameById(id));
  }

  //fonction lancé lorsqu'un joueur rejoint une game partie
  //id : id de la game rejointe
  //player : données joueur
  async joinLauncherWithId(id: string, player) {
    await this.updateCurrentGameById(id);
    this.addPlayer(player); //ajout en base de ce joueur
    this.connectToLauncher(); //affichage de la pop-up du
  }

  //affichage de la pop-up du lancher et suppression de la pop up de recherche de game
  connectToLauncher() {
    this.joinWindowVisibility = false;
    this.launcherWindowVisibility = true;
  }

  //ajout d'un joueur dans la game en cour (this.currentGame)
  //player : données joueur
  addPlayer(player) {
    this.myPlayer = new Player({ id: this.createId(), username: player.username, master: player.master });
    if (this.currentGame.players == null) {
      //initialisation du tableau de joueur en cas de création // surtout debug
      this.currentGame.players = [];
    }
    this.currentGame.players.push(this.myPlayer); //ajout du joueur dans la variable locale
    this.serverRequestService.playerJoinGame(this.currentGame.id, this.myPlayer); //ajout du joueur en base
  }

  //supprime le joueur dont l'id est en paramètre dans la game courrante
  removePlayer(idPlayer: string) {
    this.currentGame.players.filter((player) => player.id !== idPlayer);
    this.serverRequestService.removePlayerFromBase(this.currentGame.id, idPlayer);
  }

  //change l'état de la partie en cours par le state en paramètre
  changeState(newState: string) {
    const obj = { newState: newState, idGame: this.currentGame.id };
    this.serverRequestService.updateGameState(obj);
  }

  //s'occupe de vérifier si une reconnexion est possibile et si c'est le cas, met à jour les variable de base du service (currentGame, myPlayer)
  //vérifie si la game existe en base et si elle n'est pas fini, si c'est le cas la reconnexion se fait
  //gameId : l'id de l'ancienne partie
  mannageReconnection = async (gameId) => {
    const content = await this.serverRequestService.getGameById(gameId);

    if (content != null) {
      ////TEST  A SUPPRIMER
      const lastPlayer = "uyFIsmHHWr";
      /////////
      //const lastPlayer = localStorage.getItem("lastPlayerId")
      const player = content.players.find((x) => x.id == lastPlayer);
      if (player != undefined) {
        this.currentGame = new Game(content);
        this.myPlayer = new Player(player);

        return true;
      }
    }
    return false;
  };
}
