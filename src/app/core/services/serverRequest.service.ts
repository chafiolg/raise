import { Injectable } from "@angular/core";
import * as firebase from "firebase";
import { HttpClient, HttpParams, HttpHeaders } from "@angular/common/http";
import { Game } from "src/app/globalModel/game";
import { Player } from "src/app/globalModel/player";
import { InGame } from "src/app/globalModel/inGame";
import { Challenger } from "src/app/globalModel/challenger";
import { Subscriber } from "rxjs";

//require("firebase/auth");
/*import * as auth from "firebase/auth";
import * as database from "firebase/database";*/

@Injectable({
  providedIn: "root"
})
export class ServerRequestService {
  urlApi = "http://localhost:8080"; //url du serveur
  collection = null;

  constructor(private http: HttpClient) {}

  //requete http post pour créer la game en base
  //game : données de la game
  createGameOnBase(game: Game) {
    this.http.post<Game>(this.urlApi + "/createGame", game).subscribe();
  }

  //récupération de la liste de game en base
  //callback: fonction de callback
  //content renvoit un tableau d'objet (de partie)
  getExistingGameList(callback) {
    this.http.get(this.urlApi).subscribe((content) => {
      callback(content);
    });
  }

  //récupération d'une partie (objet Game) via son id
  //id : id partie
  async getGameById(id: string): Promise<Game> {
    const urlApi = `${this.urlApi}/getMainDataGame/${id}`; // URL de l'API
    const result = await this.http.get<Game>(urlApi).toPromise();
    return result;
  }

  //ajout en base d'un nouveau joueur
  //id de la partie dans laquelle ajouté le joueur
  //player : données player a ajouté
  playerJoinGame(idGame: string, player: Player) {
    const game = { idGame: idGame, player: player };
    let data = this.http.post(this.urlApi + "/addPlayer", game).subscribe();
  }

  //modifie le nom d'un joueur dans une partie
  //obj : contient le nouveau nom, plus l'id partie
  updatePlayerName(obj) {
    this.http.post(this.urlApi + "/updatePlayerName", obj).subscribe();
  }

  //fonction à optimiser, c'est sale
  async setUpObservable(path, callback) {
    if (firebase == null || !firebase.apps.length || this.collection == null) {
      let config = await this.http.get(this.urlApi + "/firebaseConfig").toPromise();
      firebase.initializeApp(config["config"]);
      this.collection = config["collection"];
    }
    firebase
      .database()
      .ref(this.collection + "/" + path + "/")
      .on("value", (snapshot) => {
        let data = snapshot.val();
        data =
          data != null
            ? Object.keys(data).map((key) => {
                return data[key];
              })
            : null;
        callback(data);
      });
  }

  removePlayerFromBase(idGame: string, idPlayer: string) {
    const obj = { idGame: idGame, idPlayer: idPlayer };
    this.http.post(this.urlApi + "/removePlayer", obj).toPromise();
  }

  updateGameState(obj) {
    this.http.post(this.urlApi + "/updateGameState", obj).toPromise();
  }

  //renvoit les data de la configuration en cour comme : la liste des collection, la liste des cartes action, l'argent de base...
  async createInitialData(gameId: string) {
    const result = await this.http
      .post<InGame>(this.urlApi + "/startingGameProcess", { id: gameId })
      .toPromise();
  }

  async getGameData(gameId: string): Promise<InGame> {
    const result = await this.http
      .post<InGame>(this.urlApi + "/getGameData", { id: gameId })
      .toPromise();
    return result;
  }

  updateInGamePlayerData(id: string, chall: Challenger): void {
    this.http
      .post<Challenger>(`${this.urlApi}/updateChall`, { idGame: id, chall: chall })
      .toPromise();
  }
  ngOnDestroy() {}
}
