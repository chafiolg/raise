import { Injectable } from "@angular/core";
import { Router } from "@angular/router";
import { ActionCard } from "src/app/globalModel/actionCard";
import { Challenger } from "src/app/globalModel/challenger";
import { CollectionCard } from "src/app/globalModel/collectionCard";
import { InGame } from "src/app/globalModel/inGame";

import { GameProcessService } from "./gameProcess.service";
import { ServerRequestService } from "./serverRequest.service";

@Injectable({
  providedIn: "root"
})
export class PlayService {
  inGame: InGame;
  myChall: Challenger;
  collectionDisplay: boolean = false;

  constructor(
    private gameProcessService: GameProcessService,
    private serverRequestService: ServerRequestService,
    private router: Router
  ) {}

  async launch() {
    await this.initGame();
    this.rejoign();
  }

  initGame() {
    this.serverRequestService.createInitialData(this.gameProcessService.currentGame.id);
  }

  async rejoign() {
    await this.updateDataFromServer();

    localStorage.setItem("currentGameId", this.gameProcessService.currentGame.id);
    localStorage.setItem("lastPlayerId", this.gameProcessService.myPlayer.id);

    this.router.navigateByUrl("/game/" + this.gameProcessService.currentGame.id);
    //les 2 prochaines lignes : test d'ajout d'artWork en base (a virer)
    this.myChall.gainArtWork(new CollectionCard(1));
    this.myChall.gainActionCard(new ActionCard());
    this.updateMyChallOnServer();
  }

  updateDataFromServer = async () => {
    this.inGame = new InGame(await this.serverRequestService.getGameData(this.gameProcessService.currentGame.id));
    this.myChall = new Challenger(
      this.inGame.challengers.find((chall) => chall.id == this.gameProcessService.myPlayer.id)
    );
  };

  updateMyChallOnServer = () => {
    this.serverRequestService.updateInGamePlayerData(this.gameProcessService.currentGame.id, this.myChall);
  };

  checkReco = async (): Promise<boolean> => {
    ////TEST  A SUPPRIMER
    const oldGameId = "JYmZoaVNOB";
    // const oldGameId = null;

    ///////
    // const oldGameId = localStorage.getItem("currentGameId");
    if (oldGameId != null) {
      const needReconnect = await this.gameProcessService.mannageReconnection(oldGameId);
      return needReconnect;
    }
    return false;
  };
}
