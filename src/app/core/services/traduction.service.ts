import { Injectable } from "@angular/core";

@Injectable({
  providedIn: "root"
})
export class TraductionService {
  languagesTranslated = ["FR", "EN"];
  languagePicked = null;

  trads = {
    ///HOME///
    testBackground: { FR: "Super background d'illustration bientot", EN: "Awesome background illustration soon" },
    creatorOnTitle: { FR: "par GuiChaf", EN: "by GuiChaf" },
    feedBackLabel: { FR: "Retours", EN: "Feedback" },
    rulesLabel: { FR: "Règles", EN: "Rules" },
    creatorProfilLabel: { FR: "Créateur", EN: "Creator" },
    languagesLabel: { FR: "Langages", EN: "Languages" },
    createGameLabel: { FR: "Créer une partie", EN: "Create new game" },
    joinGameLabel: { FR: "Rejoindre une partie", EN: "Join a game" },
    titleCreateGameWindow: { FR: "Créer votre partie", EN: "Create your game" },
    nameGameLabel: { FR: "Nom de votre partie", EN: "Name game" },
    passwordGameLabel: { FR: "Mot de passe", EN: "Password" },
    publicButtonLabel: { FR: "public", EN: "public" },
    privateButtonLabel: { FR: "privée", EN: "private" },
    createButtonLabel: { FR: "Créer", EN: "Create" },
    titleJoinGameWindow: { FR: "Choisissez une partie", EN: "Choose your game" },
    submitTitleJoinGameWindow: { FR: "Rejoindre", EN: "Join" },
    submitTitleLauncherWindow: { FR: "Lancer", EN: "Launch" },
    validateButtonLabel: { FR: "Valider", EN: "Validate" },
    cancelButtonLabel: { FR: "Annuler", EN: "Cancel" },
    connectGameLabel: { FR: "Connection à : ", EN: "Connect to : " },
    errorMessageGameNameCreation: {
      FR: "Le nom doit contenir entre 4 et 15 caractères",
      EN: "The name have to contains between 4 and 15 characters"
    },
    errorMessageGamePasswordCreation: {
      FR: "Pour une partie privée un mot de passe entre 4 et 10 caractère est requis",
      EN: "To create a private game, a password between 4 and 10 is required"
    },
    wrongPasswordLabel: {
      FR: "Mot de passe incorrect, retentes ta chance",
      EN: "Wrong password, try again"
    },
    masterCloseLauncherLabel: {
      FR: "Êtes vous sur de vouloir quitter la partie? Cela l'annulera et en éjectera tous les participants",
      EN: "Are you sure you want to leave the room? It will cancel the game and eject all players"
    },
    playerCloseLauncherLabel: {
      FR: "Êtes vous sur de vouloir quitter la partie?",
      EN: "Are you sure you want to leave the room?"
    },

    // -- Traduction des cartes de collection (nom, description et plus)
    CollectionCardName1: {
      FR: "La Joconde",
      EN: "The Mona Lisa"
    },
    CollectionCardDescription1: {},
    CollectionCardName2: {
      FR: "La Jeune fille à la perle",
      EN: "Girl with a Pearl Earring"
    },
    CollectionCardDescription2: {},
    CollectionCardName3: {
      FR: "Les saisons",
      EN: "The Four Seasons"
    },
    CollectionCardDescription3: {},
    CollectionCardName4: {
      FR: "Le masque de Toutânkhamon",
      EN: "Tutankhamun's golden mask"
    },
    CollectionCardDescription4: {},
    CollectionCardName5: {
      FR: "La liberté guidant le peuple",
      EN: "The Liberty Leading the People"
    },
    CollectionCardDescription5: {},
    CollectionCardName6: {
      FR: "La persistante de la mémoire",
      EN: "The Persistence of Memory"
    },
    CollectionCardDescription6: {},

    // -- Traduction des cartes d'actions (nom, description et plus)

    ActionCardName0: {
      FR: "Carte test",
      EN: "Test card"
    },
    ActionCardDescription0: {
      FR: "Voici une carte test, elle fait... rien a part tester",
      EN: "This is a test card, it does nothing except testing "
    },
    ActionCardLongDescription0: {
      FR: "La persistante de la mémoire",
      EN: "The Persistence of Memory"
    }
  };

  constructor() {
    if (localStorage.getItem("language") == null) {
      localStorage.setItem("language", this.languagesTranslated[0]);
    }
    this.languagePicked = localStorage.getItem("language");
  }

  getTrad(tradSearch: string) {
    let result;
    try {
      result = this.trads[tradSearch][this.languagePicked];
    } catch (e) {
      result = "traduction not found";
    }
    return result;
  }

  changeLanguage(countryCode: string) {
    localStorage.setItem("language", countryCode);
  }
}
