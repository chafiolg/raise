export class Player {
  id: string;
  username: string;
  master: boolean;

  constructor(object: any) {
    this.id = object.id;
    this.username = object.username;
    this.master = object.master ? object.master : false;
  }
}
