import { CollectionCard } from "../collectionCard";

export class LeMasqueDeToutankhamon extends CollectionCard {
  constructor() {
    super(4);
    super.valueDefault = 1500;
  }
}
