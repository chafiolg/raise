import { CollectionCard } from "../collectionCard";

export class Joconde extends CollectionCard {
  constructor() {
    super(1);
    super.valueDefault = 1500;
  }
}
