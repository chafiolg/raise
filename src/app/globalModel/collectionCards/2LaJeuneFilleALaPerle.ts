import {CollectionCard} from '../collectionCard';

export class LaJeuneFilleALaPerle extends CollectionCard{

  constructor(){
    super(2);
    super.valueDefault = 1500;
  }

}
