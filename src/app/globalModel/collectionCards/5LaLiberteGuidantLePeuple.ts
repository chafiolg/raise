import {CollectionCard} from '../collectionCard';

export class LaLiberteGuidantLePeuple extends CollectionCard{

  constructor(){
    super(5);
    super.valueDefault = 1500;
  }

}
