import {CollectionCard} from '../collectionCard';

export class LaPersistanceDeLamemoire extends CollectionCard{

  constructor(){
    super(6);
    super.valueDefault = 1500;
  }

}
