import {CollectionCard} from '../collectionCard';

export class LesSaisons extends CollectionCard{

  constructor(){
    super(3);
    super.valueDefault = 1500;
  }

}
