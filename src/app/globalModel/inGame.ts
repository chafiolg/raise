import { CollectionCard } from "./collectionCard";
import { ActionCard } from "./actionCard";
import { Challenger } from "./challenger";
import { Player } from "./player";

export class InGame {
  nbTurnLeft: Number = 0; //to define
  museumCollection: CollectionCard[] = [];
  stackArtwork: CollectionCard[] = []; //to define
  stackAction: ActionCard[] = []; //to define
  challengers: Challenger[] = [];

  //construct object with data from server
  constructor(inGame: any = {}) {
    this.nbTurnLeft = inGame.nbTurnLeft ? inGame.nbTurnLeft : 0;
    this.museumCollection = inGame.museumCollection ? inGame.museumCollection : [];
    this.stackArtwork = inGame.stackArtwork ? inGame.stackArtwork : [];
    this.stackAction = inGame.stackAction ? inGame.stackAction : [];
    this.challengers = inGame.challengers ? inGame.challengers : [];
  }
}
