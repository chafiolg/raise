export class ActionCard {
  id: number;
  keyTradName: string;
  keyTradDescription: string;
  keyTradLongDescription: string;
  illustration: string;

  constructor(cardId: number = 0) {
    this.id = cardId;
    this.keyTradName = "ActionCardName" + this.id;
    this.keyTradDescription = "ActionCardDescription" + this.id;
    this.keyTradLongDescription = "ActionCardLongDescription" + this.id;
    this.illustration =
      this.id === 0
        ? "../../assets/pictures/cardsIllustration/actionCardIllustration/defaultIllustration.png"
        : `../../assets/pictures/cardsIllustration/actionCardIllustration/${this.id}.png`;
  }
}
