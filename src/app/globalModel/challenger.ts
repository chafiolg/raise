import { CollectionCard } from "./collectionCard";
import { Injectable } from "@angular/core";
import { Player } from "./player";
import { ActionCard } from "./actionCard";
import { ServerRequestService } from "../core/services/serverRequest.service";
import { GameProcessService } from "../core/services/gameProcess.service";

@Injectable({
  providedIn: "root"
})
export class Challenger {
  gameProcessService: GameProcessService;
  id: string;
  username: string;
  monney: number;
  artWorkCollection: CollectionCard[];
  actionCollection: ActionCard[];
  master: boolean;

  constructor(player: any = {}) {
    this.id = player.id ? player.id : "";
    this.username = player.username ? player.username : "";
    this.master = player.master ? player.master : false;
    this.monney = player.monney ? player.monney : 0;
    this.artWorkCollection = player.artWorkCollection ? player.artWorkCollection : [];
    this.actionCollection = player.actionCollection ? player.actionCollection : [];
  }

  gainActionCard = (actionCard: ActionCard) => {
    this.actionCollection.push(actionCard);
  };
  gainArtWork = (collectionCard: CollectionCard) => {
    this.artWorkCollection.push(collectionCard);
  };

  updateMonney = (value: number) => {
    this.monney += value;
  };
}
