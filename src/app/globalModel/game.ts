export class Game {
  id: string;
  name: string;
  password: string = null;
  readonly access: string;
  players = [];
  state: string = null;

  constructor(obj: any) {
    this.id = obj.id;
    this.name = obj.name;
    this.password = this.password ? this.password : null;
    this.access = this.password == null ? "public" : "private";
    this.players = this.players ? this.players : [];
    this.state = this.state ? this.state : "preparation";
  }
}
