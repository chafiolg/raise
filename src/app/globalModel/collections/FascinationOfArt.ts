import { Collection } from "../collection";
import { Joconde } from "../collectionCards/1Joconde";
import { LaJeuneFilleALaPerle } from "../collectionCards/2LaJeuneFilleALaPerle";
import { LesSaisons } from "../collectionCards/3LesSaisons";
import { LeMasqueDeToutankhamon } from "../collectionCards/4LeMasqueDeToutankhamon";
import { LaLiberteGuidantLePeuple } from "../collectionCards/5LaLiberteGuidantLePeuple";
import { LaPersistanceDeLamemoire } from "../collectionCards/6LaPersistanceDeLaMemoire";

export class FascinationOrArt extends Collection {
  constructor() {
    super();
    super.idCollection = "1";
    super.name = "FascinationOfArt";
    super.backgroundSrc = "../../../assets/pictures/background/Collection.png";
    super.style = { width: "700px", height: "500px", "background-image": "url(" + this.backgroundSrc + ")" };

    super.artWorkCollection = [
      super.createCardObj(new Joconde(), 33, 289, "main"),
      super.createCardObj(new LesSaisons(), 55, 559, "main"),
      super.createCardObj(new LaJeuneFilleALaPerle(), 238, 77, "main"),
      super.createCardObj(new LeMasqueDeToutankhamon(), 383, 289, "main"),
      super.createCardObj(new LaLiberteGuidantLePeuple(), 90, 358, "main"),
      super.createCardObj(new LaPersistanceDeLamemoire(), 382, 535, "main")
    ];
  }
}
