export class CollectionCard {
  id: number; //l'id unique qui nous servira pour définir la carte
  keyTradName: string; //l'id de traduction pour obtenir le nom de la carte dans la langue courante
  keyTradDescription: string; // l'id de traduction pour obtenir la description de la carte dans la langue courante
  valueDefault: number; // valeur de la carte par défaut
  illustration: string; // chemin de l'illustration de la carte
  iconIllustration: string; //chemin de l'îcone de l'illustration de la carte

  constructor(id: number) {
    this.id = id;
    this.keyTradName = "CollectionCardName" + id;
    this.keyTradDescription = "CollectionCardDescription" + id;
    this.illustration = "../../assets/pictures/collectionCardIllustration/" + id + ".png";
    this.iconIllustration = "../../assets/pictures/collectionCardIcon/" + id + ".png";
  }
}
