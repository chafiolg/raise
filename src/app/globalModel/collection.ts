import { CollectionCard } from "./collectionCard";

export class Collection {
  name: string;
  idCollection: string;
  backgroundSrc: string;
  style: object;
  artWorkCollection;

  constructor() {}

  createCardObj(obj: CollectionCard, top: number, left: number, classe: string) {
    return {
      idCard: obj.id,
      class: classe,
      style: {
        top: top + "px",
        left: left + "px",
        "background-image": "url(../../../assets/pictures/cardsIllustration/collectionCardIcon/" + obj.id + ".png)"
      }
    };
  }
}
