import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { HttpClientModule } from "@angular/common/http";

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";

import { CoreModule } from "./core/core.module";
import { FeedbackModule } from "./pages/feedback/feedback.module";
import { HomeModule } from "./pages/home/home.module";
import { GameModule } from "./pages/game/game.module";
import { RulesModule } from "./pages/rules/rules.module";
import { ProfilModule } from "./pages/profil/profil.module";
import { GameProcessService } from "./core/services/gameProcess.service";
import { PlayService } from "./core/services/play.service";
import { ServerRequestService } from "./core/services/serverRequest.service";
import { TraductionService } from "./core/services/traduction.service";

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,
    CoreModule,
    FeedbackModule,
    HomeModule,
    GameModule,
    RulesModule,
    ProfilModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
