import { Component, OnInit, Input } from "@angular/core";
import { PlayService } from "src/app/core/services/play.service";

import { Collection } from "../../../globalModel/collection";
import { FascinationOrArt } from "../../../globalModel/collections/FascinationOfArt";

@Component({
  selector: "app-collection",
  templateUrl: "./collection.component.html",
  styleUrls: ["./collection.component.scss"]
})
export class CollectionComponent implements OnInit {
  @Input() owner: "challenger" | "museum";
  collection: Collection;

  constructor(private playService: PlayService) {}

  ngOnInit() {
    this.initPossessions();
  }

  //initialise la liste et les classes en fonction des possessions soit du joueur soit du musée
  initPossessions = () => {
    const possessions =
      this.owner == "challenger" ? this.playService.myChall.artWorkCollection : this.playService.inGame.stackArtwork;
    this.collection = new FascinationOrArt();
    possessions.forEach((value) => {
      this.collection.artWorkCollection.find((art) => art.idCard == value.id).class += " own";
    });
  };

  close = () => {
    this.playService.collectionDisplay = false;
  };
}
