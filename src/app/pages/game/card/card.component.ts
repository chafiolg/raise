import { Component, OnInit, Input } from "@angular/core";
import { TraductionService } from "src/app/core/services/traduction.service";
import { ActionCard } from "src/app/globalModel/actionCard";

export const globalShift = 50;

@Component({
  selector: "app-card",
  templateUrl: "./card.component.html",
  styleUrls: ["./card.component.scss"]
})
export class CardComponent implements OnInit {
  @Input() card: ActionCard;
  @Input() index: number; //index in the alignement of cards
  title: string;
  description: string;
  priority: number; //only used for hover method
  shift: number; //only used to determine margin
  constructor(private traductionService: TraductionService) {}

  ngOnInit() {
    this.title = this.traductionService.getTrad(this.card.keyTradName);
    this.description = this.traductionService.getTrad(this.card.keyTradDescription);
    this.shift = globalShift * this.index;
    this.priority = this.index;
  }
  updatePriority = (hover: boolean) => {
    if (hover) this.priority = 100;
    else this.priority = this.index;
  };
}
