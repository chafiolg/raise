import {
  Input,
  AfterViewChecked,
  Component,
  OnChanges,
  OnInit,
  AfterContentChecked,
  SimpleChanges
} from "@angular/core";
import { ActionCard } from "src/app/globalModel/actionCard";

@Component({
  selector: "app-private-cards-display",
  templateUrl: "./private-cards-display.component.html",
  styleUrls: ["./private-cards-display.component.scss"]
})
export class PrivateCardsDisplayComponent implements OnInit, OnChanges {
  @Input() cardCollection: ActionCard[];
  marginLeft: number;
  constructor() {}

  ngOnInit() {
    this.refreshMarginLeft();
  }

  ngOnChanges() {
    this.refreshMarginLeft();
  }
  refreshMarginLeft = (): void => {
    this.marginLeft = 200 + (10 - this.cardCollection.length) * 10;
  };
}
