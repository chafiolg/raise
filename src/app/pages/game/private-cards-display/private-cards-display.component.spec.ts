import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PrivateCardsDisplayComponent } from './private-cards-display.component';

describe('PrivateCardsDisplayComponent', () => {
  let component: PrivateCardsDisplayComponent;
  let fixture: ComponentFixture<PrivateCardsDisplayComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PrivateCardsDisplayComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PrivateCardsDisplayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
