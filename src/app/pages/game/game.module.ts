import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { GameComponent } from "./game.component";
import { CardComponent } from "./card/card.component";
import { CollectionComponent } from "./collection/collection.component";
import { PlayerDisplayComponent } from "./player-display/player-display.component";
import { PrivateCardsDisplayComponent } from "./private-cards-display/private-cards-display.component";
import { SharedModule } from "../shared/shared.module";

@NgModule({
  declarations: [
    GameComponent,
    CardComponent,
    CollectionComponent,
    PlayerDisplayComponent,
    PrivateCardsDisplayComponent
  ],
  imports: [CommonModule, SharedModule],
  exports: [GameComponent]
})
export class GameModule {}
