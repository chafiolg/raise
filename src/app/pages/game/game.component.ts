import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { GameProcessService } from "src/app/core/services/gameProcess.service";
import { PlayService } from "src/app/core/services/play.service";
import { ActionCard } from "src/app/globalModel/actionCard.js";

import { Vivify, $v } from "../../../../node_modules/vivifyJS-master/dist/js/vivify.min.js";

@Component({
  selector: "app-game",
  templateUrl: "./game.component.html",
  styleUrls: ["./game.component.scss"]
})
export class GameComponent {
  private collectionOwner: "challenger" | "museum" = "museum";
  constructor(
    private playService: PlayService,
    private gameProcessService: GameProcessService,
    private router: Router
  ) {
    if (!this.checkDataValidity()) this.router.navigateByUrl("/home");
  }

  checkDataValidity = (): boolean => {
    if (
      this.playService.inGame &&
      this.playService.myChall &&
      this.gameProcessService.currentGame &&
      this.gameProcessService.myPlayer
    ) {
      return true;
    }
    return false;
  };

  newCard() {
    this.playService.myChall.gainActionCard(new ActionCard());
    this.playService.updateMyChallOnServer();
  }

  /*ngAfterViewInit() {
    // select element to animate
      $v(".ennemy-div")

    // append animation
    // you can animate many properties in the same animation. They will get animated at the same time
    .animate({
    "opacity":"1",
    "transform":"rotate(360deg)"
    },{
        duration: 5000,
        delay: 200,
        easing: "ease-in"
    })
// start the animation
      $v(".ennemy-div").begin();
  }
  }*/

  displayCollection(owner: string) {
    this.collectionOwner = owner ? "challenger" : "museum";
    this.playService.collectionDisplay = !this.playService.collectionDisplay;
  }
}
