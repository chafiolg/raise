import { Component, OnInit, Input } from "@angular/core";
import { PlayService } from "src/app/core/services/play.service";

import { Challenger } from "../../../globalModel/challenger";

@Component({
  selector: "app-player-display",
  templateUrl: "./player-display.component.html",
  styleUrls: ["./player-display.component.scss"]
})
export class PlayerDisplayComponent implements OnInit {
  @Input() challenger: Challenger;
  ownPlayer: boolean;
  position = {};

  constructor(private playService: PlayService) {}

  ngOnInit() {
    this.ownPlayer = this.playService.myChall.id == this.challenger.id;
    this.position = this.findPosition();
  }

  findPosition() {
    let position;
    if (this.ownPlayer) {
      position = {
        left: "50%",
        bottom: "0%",
        transform: "translate(-50%,-0%)"
      };
    } else {
      const ordChalls = this.playService.inGame.challengers.filter((chall) => chall.master == false);
      const yourIndex = ordChalls.findIndex((chall) => chall.id == this.challenger.id);
      position = this.positions(ordChalls.length, yourIndex);
    }
    return position;
  }
  positions(nbPlayers, index) {
    const result = {
      "2": [
        {
          left: "12%",
          top: "14%"
        },
        {
          left: "88%",
          top: "14%"
        }
      ],
      "3": [
        {
          left: "12%",
          top: "26%"
        },
        {
          left: "50%",
          top: "12%"
        },
        {
          left: "88%",
          top: "26%"
        }
      ],
      "4": [
        {
          left: "8%",
          top: "26%"
        },
        {
          left: "35%",
          top: "12%"
        },
        {
          left: "65%",
          top: "12%"
        },
        {
          left: "92%",
          top: "26%"
        }
      ],
      "5": [
        {
          left: "5%",
          top: "32%"
        },
        {
          left: "25%",
          top: "21%"
        },
        {
          left: "50%",
          top: "10%"
        },
        {
          left: "75%",
          top: "21%"
        },
        {
          left: "95%",
          top: "32%"
        }
      ]
    };

    return result[nbPlayers][index];
  }
}
