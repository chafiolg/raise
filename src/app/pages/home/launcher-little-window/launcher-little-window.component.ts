import * as firebase from "firebase";
import { Router } from "@angular/router";
import { Component, OnInit } from "@angular/core";

import { Game } from "../../../globalModel/game";
import { TraductionService } from "src/app/core/services/traduction.service";
import { GameProcessService } from "src/app/core/services/gameProcess.service";
import { ServerRequestService } from "src/app/core/services/serverRequest.service";
import { PlayService } from "src/app/core/services/play.service";

@Component({
  selector: "app-launcher-little-window",
  templateUrl: "./launcher-little-window.component.html",
  styleUrls: ["./launcher-little-window.component.scss"]
})
export class LauncherLittleWindowComponent implements OnInit {
  submitTitle: string;
  closePopUp: boolean = false;
  closeMessageId: string = "";

  constructor(
    private traductionService: TraductionService,
    private gameProcessService: GameProcessService,
    private serverRequestService: ServerRequestService,
    private playService: PlayService
  ) {
    this.submitTitle = this.traductionService.getTrad("submitTitleLauncherWindow");
  }

  async ngOnInit() {
    await this.serverRequestService.setUpObservable(this.gameProcessService.currentGame.id + "/players", (snapshot) => {
      this.gameProcessService.currentGame.players = snapshot;
    });
  }

  click() {
    if (this.gameProcessService.myPlayer.master) {
      this.playService.launch();
    }
  }

  saveName(event) {
    const thisPlayer = this.gameProcessService.currentGame.players.find(
      (data) => data.id == this.gameProcessService.myPlayer.id
    );
    thisPlayer.username = event.target.value;
    this.gameProcessService.myPlayer.username = event.target.value;

    this.serverRequestService.updatePlayerName({
      idGame: this.gameProcessService.currentGame.id,
      idPlayer: this.gameProcessService.myPlayer.id,
      newName: event.target.value
    });
  }

  removePlayer(playerId: string) {
    this.gameProcessService.removePlayer(playerId);
  }

  onValidate(agreed: boolean) {
    if (agreed) {
      if (this.gameProcessService.myPlayer.master) {
        this.gameProcessService.changeState("closing");
      }
      this.removePlayer(this.gameProcessService.myPlayer.id);
      this.closePopUp = false;
      this.gameProcessService.launcherWindowVisibility = false;
    } else {
      this.closePopUp = false;
    }
  }

  close() {
    if (this.gameProcessService.myPlayer.master) {
      this.closeMessageId = "masterCloseLauncherLabel";
    } else {
      this.closeMessageId = "playerCloseLauncherLabel";
    }
    this.closePopUp = true;
  }
}
