import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LauncherLittleWindowComponent } from './launcher-little-window.component';

describe('LauncherLittleWindowComponent', () => {
  let component: LauncherLittleWindowComponent;
  let fixture: ComponentFixture<LauncherLittleWindowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LauncherLittleWindowComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LauncherLittleWindowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
