import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { HomeComponent } from "./home.component";
import { PasswordValidationPopupComponent } from "./password-validation-popup/password-validation-popup.component";
import { LauncherLittleWindowComponent } from "./launcher-little-window/launcher-little-window.component";
import { JoinGameLittleWindowComponent } from "./join-game-little-window/join-game-little-window.component";
import { CreateGameLittleWindowComponent } from "./create-game-little-window/create-game-little-window.component";
import { SharedModule } from "../shared/shared.module";
import { ReactiveFormsModule } from "@angular/forms";

@NgModule({
  declarations: [
    HomeComponent,
    PasswordValidationPopupComponent,
    LauncherLittleWindowComponent,
    JoinGameLittleWindowComponent,
    CreateGameLittleWindowComponent
  ],
  imports: [CommonModule, SharedModule, ReactiveFormsModule],
  exports: [HomeComponent]
})
export class HomeModule {}
