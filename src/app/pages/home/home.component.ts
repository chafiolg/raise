import { Component, OnInit } from "@angular/core";
import { GameProcessService } from "src/app/core/services/gameProcess.service";
import { PlayService } from "src/app/core/services/play.service";
import { TraductionService } from "src/app/core/services/traduction.service";

import { CreateGameLittleWindowComponent } from "./create-game-little-window/create-game-little-window.component";
import { JoinGameLittleWindowComponent } from "./join-game-little-window/join-game-little-window.component";
import { LauncherLittleWindowComponent } from "./launcher-little-window/launcher-little-window.component";

@Component({
  selector: "app-home",
  templateUrl: "./home.component.html",
  styleUrls: ["./home.component.scss"]
})
export class HomeComponent implements OnInit {
  creatorOnTitle: string;
  backgroundMessage: string;
  titleMessage: string;

  constructor(
    private traductionService: TraductionService,
    private gameProcessService: GameProcessService,
    private playService: PlayService
  ) {
    this.backgroundMessage = this.traductionService.getTrad("testBackground");
    this.creatorOnTitle = this.traductionService.getTrad("creatorOnTitle");
  }

  ngOnInit() {}
  leaveHome() {
    console.log("close");
  }
}
