import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PasswordValidationPopupComponent } from './password-validation-popup.component';

describe('PasswordValidationPopupComponent', () => {
  let component: PasswordValidationPopupComponent;
  let fixture: ComponentFixture<PasswordValidationPopupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PasswordValidationPopupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PasswordValidationPopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
