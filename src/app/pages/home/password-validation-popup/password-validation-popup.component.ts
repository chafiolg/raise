import { Component, OnInit, Input } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { TraductionService } from "src/app/core/services/traduction.service";
import { Game } from "../../../globalModel/game";

@Component({
  selector: "app-password-validation-popup",
  templateUrl: "./password-validation-popup.component.html",
  styleUrls: ["./password-validation-popup.component.scss"]
})
export class PasswordValidationPopupComponent implements OnInit {
  @Input() gameConnection: Game;
  @Input() parentComponent;

  title: string;
  passwordGameLabel: string;
  validateButtonLabel: string;
  errorMessage: "";

  formPasswordValidation: FormGroup;
  constructor(private formBuilder: FormBuilder, private traductionService: TraductionService) {}

  ngOnInit() {
    this.title = this.traductionService.getTrad("connectGameLabel") + this.gameConnection.name;
    this.passwordGameLabel = this.traductionService.getTrad("passwordGameLabel") + " :";
    this.validateButtonLabel = this.traductionService.getTrad("validateButtonLabel");

    this.initFormPassword();
  }

  initFormPassword() {
    this.formPasswordValidation = this.formBuilder.group({
      passwordInput: ["", Validators.required]
    });
  }

  validPassword() {
    if (this.checkPassword()) {
      this.parentComponent.goToLauncher(this.gameConnection.id);
    } else {
      this.errorMessage = this.traductionService.getTrad("wrongPasswordLabel");
    }
  }

  updatePassword() {
    this.errorMessage = "";
  }
  checkPassword() {
    //a remplacer par une fonction plus sécurisé

    let password = this.formPasswordValidation.get("passwordInput").value;
    return password == this.gameConnection.password;
  }

  close() {
    this.parentComponent.gameConnection = null;
  }
}
