import { Component, OnInit } from "@angular/core";
import { GameProcessService } from "src/app/core/services/gameProcess.service";
import { ServerRequestService } from "src/app/core/services/serverRequest.service";
import { TraductionService } from "src/app/core/services/traduction.service";

import { Game } from "../../../globalModel/game";

@Component({
  selector: "app-join-game-little-window",
  templateUrl: "./join-game-little-window.component.html",
  styleUrls: ["./join-game-little-window.component.scss"]
})
export class JoinGameLittleWindowComponent implements OnInit {
  title: string;
  submitTitle: string;
  recordedGames: Game[];
  gameConnection: Game = null;

  constructor(
    private traductionService: TraductionService,
    private gameProcessService: GameProcessService,
    private serverRequestService: ServerRequestService
  ) {}

  ngOnInit() {
    this.title = this.traductionService.getTrad("titleJoinGameWindow");
    this.submitTitle = this.traductionService.getTrad("submitTitleJoinGameWindow");
    this.getExistingGameList();
  }

  getExistingGameList() {
    this.serverRequestService.getExistingGameList((content) => {
      this.recordedGames = content;
    });
  }

  tryJoinLobby(game: Game) {
    if (this.gameConnection == null) {
      if (game.access == "public") {
        this.goToLauncher(game.id);
      } else {
        this.gameConnection = game;
      }
    }
  }

  goToLauncher(id: string) {
    this.gameConnection = null;
    this.gameProcessService.joinWindowVisibility = false;
    this.gameProcessService.joinLauncherWithId(id, { username: "Random Challenger", master: false });
  }

  reload() {
    this.getExistingGameList();
  }

  close() {
    this.gameProcessService.joinWindowVisibility = false;
  }
}
