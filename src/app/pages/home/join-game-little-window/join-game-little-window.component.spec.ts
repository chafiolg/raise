import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { JoinGameLittleWindowComponent } from './join-game-little-window.component';

describe('JoinGameLittleWindowComponent', () => {
  let component: JoinGameLittleWindowComponent;
  let fixture: ComponentFixture<JoinGameLittleWindowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JoinGameLittleWindowComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JoinGameLittleWindowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
