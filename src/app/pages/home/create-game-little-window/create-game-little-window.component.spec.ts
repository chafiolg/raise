import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateGameLittleWindowComponent } from './create-game-little-window.component';

describe('CreateGameLittleWindowComponent', () => {
  let component: CreateGameLittleWindowComponent;
  let fixture: ComponentFixture<CreateGameLittleWindowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateGameLittleWindowComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateGameLittleWindowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
