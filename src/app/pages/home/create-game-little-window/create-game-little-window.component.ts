import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { GameProcessService } from "src/app/core/services/gameProcess.service";
import { TraductionService } from "src/app/core/services/traduction.service";

@Component({
  selector: "app-create-game-little-window",
  templateUrl: "./create-game-little-window.component.html",
  styleUrls: ["./create-game-little-window.component.scss"]
})
export class CreateGameLittleWindowComponent implements OnInit {
  //label
  title: string;
  nameGameLabel: string;
  passwordGameLabel: string;
  publicButtonLabel: string;
  privateButtonLabel: string;
  createButtonLabel: string;
  //
  //variable
  formCreation: FormGroup;
  gameAccess: string = "public"; //public or private
  //
  errorMessage = "";

  constructor(
    private formBuilder: FormBuilder,
    private traductionService: TraductionService,
    private gameProcessService: GameProcessService
  ) {}

  ngOnInit() {
    //traduction
    this.title = this.traductionService.getTrad("titleCreateGameWindow");
    this.nameGameLabel = this.traductionService.getTrad("nameGameLabel");
    this.passwordGameLabel = this.traductionService.getTrad("passwordGameLabel");
    this.publicButtonLabel = this.traductionService.getTrad("publicButtonLabel");
    this.privateButtonLabel = this.traductionService.getTrad("privateButtonLabel");
    this.createButtonLabel = this.traductionService.getTrad("createButtonLabel");
    //
    this.initFormCreate();
  }

  initFormCreate() {
    this.formCreation = this.formBuilder.group({
      gameName: ["", [Validators.required, Validators.minLength(4), Validators.maxLength(15)]],
      password: ["", [Validators.required, Validators.minLength(4), Validators.maxLength(10)]]
    });
  }

  create() {
    if (this.formCreation.controls.gameName.invalid) {
      this.errorMessage = this.traductionService.getTrad("errorMessageGameNameCreation");
      return;
    } else if (this.formCreation.controls.password.invalid && this.gameAccess == "private") {
      this.errorMessage = this.traductionService.getTrad("errorMessageGamePasswordCreation");
      return;
    }

    let gameName = this.formCreation.get("gameName").value;
    let password = this.gameAccess == "public" ? null : this.formCreation.get("password").value;
    //Peut etre à regrouper
    this.gameProcessService.createTheGame(gameName, password);
    this.gameProcessService.joinLauncherWithId(this.gameProcessService.currentGame.id, {
      username: "MasterRaiser",
      master: true
    });

    this.close();
  }

  close() {
    this.gameProcessService.createWindowVisibility = false;
  }

  changeAccess(newAccess: string) {
    this.gameAccess = newAccess;
  }
}
