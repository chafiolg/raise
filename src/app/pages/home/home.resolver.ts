import { Injectable } from "@angular/core";
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from "@angular/router";
import { PlayService } from "src/app/core/services/play.service";
import { HomeModule } from "./home.module";

@Injectable({ providedIn: "root" })
export class HomeResolver implements Resolve<boolean> {
  constructor(private playService: PlayService) {}

  async resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Promise<boolean> {
    const result = await this.playService.checkReco();
    if (result) {
      this.playService.rejoign();
    }
    return result;
  }
}
