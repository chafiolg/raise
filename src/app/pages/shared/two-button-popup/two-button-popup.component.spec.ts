import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TwoButtonPopupComponent } from './two-button-popup.component';

describe('TwoButtonPopupComponent', () => {
  let component: TwoButtonPopupComponent;
  let fixture: ComponentFixture<TwoButtonPopupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TwoButtonPopupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TwoButtonPopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
