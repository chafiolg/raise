import { Component, EventEmitter, OnInit, Input, Output } from "@angular/core";
import { GameProcessService } from "src/app/core/services/gameProcess.service";
import { TraductionService } from "src/app/core/services/traduction.service";

@Component({
  selector: "app-two-button-popup",
  templateUrl: "./two-button-popup.component.html",
  styleUrls: ["./two-button-popup.component.scss"]
})
export class TwoButtonPopupComponent implements OnInit {
  @Input() messageId: string;
  @Output() validated = new EventEmitter<boolean>();

  message: string;
  validateButtonLabel: string;
  cancelButtonLabel: string;

  constructor(private traductionService: TraductionService, private gameProcessService: GameProcessService) {}

  ngOnInit() {
    this.message = this.traductionService.getTrad(this.messageId);
    this.validateButtonLabel = this.traductionService.getTrad("validateButtonLabel");
    this.cancelButtonLabel = this.traductionService.getTrad("cancelButtonLabel");
  }

  valid(valid: boolean) {
    this.validated.emit(valid);
  }
}
