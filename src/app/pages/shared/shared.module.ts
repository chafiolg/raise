import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { TwoButtonPopupComponent } from "./two-button-popup/two-button-popup.component";
import { MessagePopupComponent } from "./message-popup/message-popup.component";
import { ButtonSideComponent } from "./button-side/button-side.component";

@NgModule({
  declarations: [TwoButtonPopupComponent, MessagePopupComponent, ButtonSideComponent],
  imports: [CommonModule],
  exports: [TwoButtonPopupComponent, MessagePopupComponent, ButtonSideComponent]
})
export class SharedModule {}
