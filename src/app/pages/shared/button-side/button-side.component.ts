import { Component, OnInit, Input } from "@angular/core";
import { trigger, state, style, transition, animate, keyframes } from "@angular/animations";
import { Router } from "@angular/router";
import { TraductionService } from "src/app/core/services/traduction.service";
import { GameProcessService } from "src/app/core/services/gameProcess.service";

@Component({
  selector: "app-button-side",
  templateUrl: "./button-side.component.html",
  styleUrls: ["./button-side.component.scss"],
  animations: [
    trigger("hoverAnimation", [
      state(
        "mouseOn",
        style({
          "padding-top": "50px"
        })
      ),
      state(
        "mouseOff",
        style({
          "padding-top": "15px"
        })
      ),
      transition("mouseOn <=> mouseOff", animate("300ms ease-in"))
    ])
  ]
})
export class ButtonSideComponent implements OnInit {
  @Input() theme: string;

  themeLabel: string;
  img = []; //width & height must be 50px
  onClick = function () {};
  onClickImg = function (countryCode: string = "") {};
  state: string = "mouseOff";

  constructor(
    private traductionService: TraductionService,
    private router: Router,
    private gameProcessService: GameProcessService
  ) {}

  ngOnInit() {
    this.themeLabel = this.traductionService.getTrad(this.theme + "Label");

    switch (this.theme) {
      case "createGame":
        this.onClick = function () {
          this.gameProcessService.launcherWindowVisibility = false;
          this.gameProcessService.joinWindowVisibility = false;
          this.gameProcessService.createWindowVisibility = true;
        };
        break;

      case "joinGame":
        this.onClick = function () {
          this.gameProcessService.launcherWindowVisibility = false;
          this.gameProcessService.createWindowVisibility = false;
          this.gameProcessService.joinWindowVisibility = true;
        };
        break;

      case "rules":
        this.img = [{ src: "assets/pictures/icons/home/rulesIcon.png" }];
        this.onClick = function () {
          this.router.navigateByUrl("/rules");
        };
        break;
      case "feedBack":
        this.img = [{ src: "assets/pictures/icons/home/penIcon.png" }];
        this.onClick = function () {
          this.router.navigateByUrl("/feedback");
        };
        break;
      case "creatorProfil":
        this.img = [{ src: "assets/pictures/icons/home/profilIcon.png" }];
        this.onClick = function () {
          this.router.navigateByUrl("/profil");
        };
        break;
      case "languages":
        this.img = [
          { src: "assets/pictures/icons/home/britishFlagIcon.png", countryCode: "EN" },
          { src: "assets/pictures/icons/home/frenchFlagIcon.png", countryCode: "FR" }
        ];
        this.onClickImg = function (countryCode: string) {
          this.traductionService.changeLanguage(countryCode);
          location.reload();
        };
        break;
    }
  }

  hoverComp(state: boolean) {
    this.state = state ? "mouseOn" : "mouseOff";
  }
}
