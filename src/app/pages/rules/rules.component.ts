import { Component, OnInit } from "@angular/core";
import { PlayService } from "src/app/core/services/play.service";

@Component({
  selector: "app-rules",
  templateUrl: "./rules.component.html",
  styleUrls: ["./rules.component.scss"]
})
export class RulesComponent implements OnInit {
  constructor(private playService: PlayService) {}

  ngOnInit() {}
}
