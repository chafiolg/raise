import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { ProfilComponent } from "./profil.component";
import { SharedModule } from "../shared/shared.module";

@NgModule({
  declarations: [ProfilComponent],
  imports: [CommonModule, SharedModule],
  exports: [ProfilComponent]
})
export class ProfilModule {}
