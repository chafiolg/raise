import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { HomeComponent } from "./pages/home/home.component";
import { ProfilComponent } from "./pages/profil/profil.component";
import { GameComponent } from "./pages/game/game.component";
import { FeedbackComponent } from "./pages/feedback/feedback.component";
import { RulesComponent } from "./pages/rules/rules.component";
import { HomeResolver } from "./pages/home/home.resolver";

const routes: Routes = [
  { path: "game/:id", component: GameComponent },
  { path: "feedback", component: FeedbackComponent },
  { path: "rules", component: RulesComponent },
  { path: "profil", component: ProfilComponent },
  {
    path: "**",
    component: HomeComponent,
    resolve: {
      home: HomeResolver
    }
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
